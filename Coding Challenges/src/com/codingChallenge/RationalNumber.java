package com.codingChallenge;

import java.util.Scanner;

public class RationalNumber {

	public static void main(String[] args) {
		Scanner jin = new Scanner(System.in);
		int N;
		
		N = jin.nextInt();
		
		System.out.println(rationalNumber(N));
	}
	
	public static int rationalNumber(int N) {
		
		
		int result = 0, temp = 1, subTemp;
		boolean isRational = true;
		
		while(temp <= N) {
			String longInput = Integer.toString(temp);
			while(isRational) {
				for(int x = 0; x < longInput.length(); x++) {
					subTemp = Integer.parseInt(longInput);
					if(temp != subTemp) {
						isRational = false;
						break;
					}else {
						result ++;
						
					}
				}
			}
			isRational = true;
			temp++;
		}		
		
		return result;
	}
}

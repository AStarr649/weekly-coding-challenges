package com.weekthree;

import java.util.HashMap;

public class LockerProblem {

	public static void main(String[] args) {
		
		HashMap<Integer, Boolean> lockers = new HashMap<>();
		int total = 0;
		
		for(int x = 0; x < 100; x++) {
			lockers.put(x + 1, false);
		}
		
		for(int i = 1; i <= 100; i++) {
			for(int x = i; x <= 100; x += i) {
				if(lockers.get(x) == false) {
					lockers.replace(x, true);
				}else if(lockers.get(x) == true) {
					lockers.replace(x, false);
				}
				
			}
		}
		
		for(Integer t : lockers.keySet()) {
			if(lockers.get(t) == true) {
				total++;
			}
		}
		
		System.out.println("The total number of open lockers are: " + total);
	}
}

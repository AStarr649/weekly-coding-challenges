package com.weekone;

import java.util.LinkedList;

public class CodingChallenge_One {
	
	public static void main(String[] args) {
		LinkedList<Integer> list1 = new LinkedList<Integer>();
		LinkedList<Integer> list2 = new LinkedList<Integer>();
		
		int answer = 0;
		boolean found = false;
		
		list1.add(3);
		list1.add(7);
		list1.add(8);
		list1.add(10);
		
		list2.add(99);
		list2.add(1);
		list2.add(8);
		list2.add(10);
		
		while(!found) {
			for (int i = 0; i < list1.size(); i++) {
	            for (int j = 0; j < list2.size(); j++) {
					if(list1.get(i).equals(list2.get(j))) {
						answer = list1.get(i);
						found = true;
						break;
					}
				}
	            if(found == true) {
	            	break;
	            }
	        }
		}
		
		System.out.println(answer);
		
	}

}
